// types
import { ADD_TODO, REMOVE_TODO, UPDATE_TODO } from '../actions';

const INITIAL_STATE = [];

export default function reducerTodo(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        {
          id: Math.random(),
          content: action.content,
        },
      ];
    case REMOVE_TODO:
      return state.filter(({ id }) => id !== action.id);
    case UPDATE_TODO:
      return state.map(todo => (todo.id === action.id ? { ...todo, ...action } : todo));
    default:
      return state;
  }
}
