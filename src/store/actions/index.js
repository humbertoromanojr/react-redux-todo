// types
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'REMOVE_TODO'
export const UPDATE_TODO = 'UPDATE_TODO'

// actionsCreators
export function addTodo(content) {
  return {
    type: ADD_TODO,
    content
  }
}

export function removeTodo(id) {
  return {
    type: REMOVE_TODO,
    id
  }
}

