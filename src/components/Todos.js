import React, { Component } from 'react';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../store/actions'

class Todos extends Component {

  state = {
    todos: []
  }

  removeTodo = (id) => {
    console.log(this.props)
    this.props.removeTodo(this.props.id);
  };

  render() {
    return (
      <div>
        {
          this.props.todos.map(todo => (
           <div key={todo.id}>
              <span>{todo.id} - {todo.content}</span>
              <button
              type="button"
              onClick={() => this.removeTodo(todo.id)}
              >Delete</button>
           </div>
          ))
        }
      </div>
    )
  }

}

const mapStateToProps = state => ({
  todos: state.todos
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Todos)
