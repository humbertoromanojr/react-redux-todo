import React, { Component } from 'react';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as actions from '../store/actions'

class AddTodo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      todos: [],
    };
  }

  handleChange = (event) => {
    const { state } = this;

    state[event.target.name] = event.target.value;

    this.setState({
      state,
    });
  };

  handleSubmit = (event) => {
    // change behavior browser default
    event.preventDefault();

    this.props.addTodo(this.state.content);

    // clean input, after add
    this.setState({ content: '' })
  }

  render() {
    return (
      <div>

      <form onSubmit={this.handleSubmit}>
        <label>Add new todo</label>
        <br />
        <input
          type="text"
          onChange={(event) => this.setState({ content: event.target.value })}
          value={this.state.content}
        />
        <button type="submit" onClick={this.handleSubmit}>Add todo</button>
      </form>
    </div>
    )
  }
}

const mapStateToProps = state => ({
todos: state.todos
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo)
