import React, { Component } from 'react';

import Todos from './components/Todos'
import AddTodo from './components/AddTodo'

class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          TODO: REACT & REDUX
        </header>
        {/* passing the state via props */}
        <AddTodo addTodo={this.addTodo} />
        <hr />
        {/* passing the state via props */}
        Todo list:
        <br />
        {/* passing the state via props */}
        <Todos />

      </div>
    );
  }
}

export default App
